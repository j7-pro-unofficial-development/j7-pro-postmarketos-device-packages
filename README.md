This repo is for device and kernel packages of Samsung Galaxy J7 Pro, as well as flashable zips for installation on the device with custom recovery.

refer to https://wiki.postmarketos.org/wiki/Samsung_Galaxy_J7_Pro_(samsung-j7y17lte)#Installation for installation instructions.

Also please read the other sections on the page for more info on how to get things work.

If you need more help on specific issues or you need to contact me, I'm currently active in postmarketOS matrix group.

See the instructions on that too https://wiki.postmarketos.org/wiki/Matrix_and_IRC

I'm in the postmarketOS main room and pmOS porting, Mainline, Development and offtopic rooms too.

I don't want to deal with merge requests right now, so I found this solution. I know this is not the right way but git is so frustrating to manage right now.